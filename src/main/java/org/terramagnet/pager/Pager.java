/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.pager;

import java.io.Serializable;

/**
 * 分页信息.
 *
 * @author LEE
 */
public interface Pager extends Serializable {

    /**
     * 分页器默认每页数据量----12
     */
    public int DEFAULT_PAGESIZE = 12;

    /**
     * 返回当前页码.
     *
     * @return 若不分页返回当前页码
     */
    public int getCurrentPage();

    /**
     * 每页的数据量.
     *
     * @see #DEFAULT_PAGESIZE 默认
     *
     * @return 若不分页返回每页的数据量
     */
    public int getPageSize();

    /**
     * 当前页的第一条数据在不分页的结果集中的索引号. 
     * @return 索引号. 从{@code 0}开始.
     */
    public int getFirstIndex();
    /**
     * 若不分页返回的结果集大小.
     *
     * @return 若不分页返回的结果集大小
     */
    public int getTotalSize();

    /**
     * 设置不分页的数据总数.
     *
     * 在数据库分页查询后需调用此方法显示改变总数.
     */
    public void setTotalSize(int total);

    /**
     * 返回JSON字符串以方便浏览器段生成分页信息.
     *
     * <p>返回的JSON数据包含了源数据总数（totalSize）、每页数据量（pageSize）以及当前页码（currentPage）信息。</p>
     *
     * @return JSON字符串<strong>（不包含数据结果集）</strong>
     */
    public String toJson();
}
