/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.pager;

import java.util.List;

/**
 * 一个存储了所有结果，并能根据当前分页属性返回当前页的结果集的分页器. 具有缓存特性。
 *
 * @author LEE
 * @deprecated 不合规范的{@link #setTotalSize(int) }实现；不明的应用场景。
 */
public class ListPager<T> implements Pager {

    private int currentPage = 1;
    private int pageSize = DEFAULT_PAGESIZE;
    private List<T> source;

    public ListPager() {
    }

    public ListPager(List<T> source) {
        this.source = source;
    }

    public ListPager(int pageSize, List<T> source) {
        this.source = source;
        this.pageSize = pageSize;
    }

    public ListPager(int pageSize, int currentPage, List<T> source) {
        this.source = source;
        if (currentPage > 1) {
            this.currentPage = currentPage;
        }
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public int getTotalSize() {
        return source == null ? 0 : source.size();
    }

    @Override
    public int getFirstIndex() {
        return (currentPage - 1) * pageSize;
    }

    /**
     * @deprecated 方法始终抛出异常.
     */
    @Override
    public void setTotalSize(int total) {
        throw new UnsupportedOperationException("use ListPager#setSource(List<T> source) instead!");
    }

    public List<T> getSource() {
        return source;
    }

    /**
     * 设置不分页时的全部结果集.
     *
     * @param source 全部结果
     */
    public void setSource(List<T> source) {
        this.source = source;
    }

    public void setCurrentPage(int currentPage) {
        if (currentPage > 1) {
            this.currentPage = currentPage;
        }
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 返回当前页的结果集.
     *
     * @return 前页的结果集
     */
    public List<T> listCurrentData() {
        if (source == null) {
            throw new IllegalStateException("分页器尚未初始化，列表源为空！");
        }
        int fromIndex = (currentPage - 1) * pageSize;
        int toIndex = fromIndex + pageSize;
        toIndex = toIndex > source.size() ? source.size() : toIndex;
        return source.subList(fromIndex, toIndex);
    }

    @Override
    public String toJson() {
        return "{totalSize:" + getTotalSize() + ",pageSize:" + pageSize + ",currentPage:" + currentPage + "}";
    }

    /**
     * {@inheritDoc }. 与{@link #toJson() }等价
     *
     * @return JSON字符串
     */
    @Override
    public String toString() {
        return toJson();
    }
}
