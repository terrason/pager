/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.pager;

/**
 * 空分页器实现. <p>它不执行分页处理，始终处于不分页状态。</p>
 *
 * @author LEE
 */
public final class NonePager<E> extends JdbcPagerResult<E> {

    public NonePager() {
        setPageAvailable(false);
    }

    /**
     * 不包装参数表示的SQL语句，直接返回原句.
     */
    @Override
    public String wrapSql(String selectSql) {
        return selectSql;
    }

    @Override
    public int getCurrentPage() {
        return 1;
    }

    @Override
    public int getPageSize() {
        return 20;
    }

    /**
     * 直接返回参数.
     */
    @Override
    public StringBuilder wrapSql(StringBuilder selectSql) {
        return selectSql;
    }
}
